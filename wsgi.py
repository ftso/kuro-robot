import sys
sys.path.append('/home/pi/Desktop')

from kuro import application

if __name__ == "__main__":
  application.run(debug=True, threaded=True)
