from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField
from wtforms.validators import DataRequired

class LoginPage(FlaskForm):
	username = StringField('Username', validators=[DataRequired()])
	password = StringField('Password', validators=[DataRequired()])
	submit = SubmitField('Submit')
	

class LogoutPage(FlaskForm):
	submit = SubmitField('Logout')
