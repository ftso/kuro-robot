import sys
sys.path.append('/home/pi/Desktop')

from kuro import application
from robot import robot_util
import time
from camera.camera_pi import Camera
from flask import render_template, Response, redirect, url_for, flash, request, session, jsonify
from flask_login import current_user, login_user, logout_user, login_required
from login_page_def import LoginPage, LogoutPage
from werkzeug.urls import url_parse
from sqlalchemy import asc, desc
from kuro.db_models import User, LoginHistory
from robot_controller_form import RobotControllerForm
import db_utils
from datetime import datetime, timedelta

robot = robot_util.Robot()
time.sleep(1)

@application.route('/robot_controller', methods=['GET', 'POST'])
@login_required
def robot_controller():
    logout_page = LogoutPage()
    if (logout_page.is_submitted() and logout_page.submit.data):
        logout_user()
        return redirect(url_for('login'))
      
    return render_template('controller_v2.html', logout=logout_page)

@application.route('/forward', methods=['GET', 'POST'])
def forward():
    numSteps = request.form['num_steps']
    for i in range(int(numSteps)):
	    robot.walkOneStep()
    return jsonify({'data': "Walked forward " + numSteps})
    
@application.route('/right', methods=['GET', 'POST'])
def right():
    numSteps = request.form['num_steps']
    for i in range(int(numSteps)):
	    robot.turnRightOneStep()
    return jsonify({'data': "Turned right " + numSteps})
    
@application.route('/left', methods=['GET', 'POST'])
def left():
    numSteps = request.form['num_steps']
    for i in range(int(numSteps)):
	    robot.turnLeftOneStep()
    return jsonify({'data': "Turned left " + numSteps})
    
@application.route('/', methods=['GET', 'POST'])
def login():
  if (current_user.is_authenticated):
    return redirect(url_for('robot_controller'))
  login_page = LoginPage()
  
  if (login_page.is_submitted()):
    user = User.query.filter_by(username=login_page.username.data).first()
    if (user is None or not user.check_password(login_page.password.data)):
      flash('Invalid username or password')
      return redirect(url_for('login'))
    
    login_user(user)
    db_utils.insertRow(LoginHistory(username=user.username))
    next_page = request.args.get('next')
    if (not next_page):
      next_page = url_for('robot_controller')
      
    return redirect(next_page)
    
  return render_template('login.html', login=login_page)

@application.route('/login_history')
@login_required
def login_history():
  login_history_list = LoginHistory.query.order_by(desc(LoginHistory.login_timestamp))
  return render_template('login_history.html', history = login_history_list)
   
@application.route('/camera_stream')
@login_required
def camera_stream():
  return render_template('camera_stream.html')

def gen(camera):
  """Video streaming generator function."""
  while True:
    frame = camera.get_frame()
    yield (b'--frame\r\n'
      b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

@application.route('/video_feed')
def video_feed():
  print("videofeed")
  """Video streaming route. Put this in the src attribute of an img tag."""
  return Response(gen(Camera()),
    mimetype='multipart/x-mixed-replace; boundary=frame')
    
@application.before_request
def before_request():
  # setting flask.session.permanent forces flask to expire after certain amount of time.
  session.permanent = True
  application.permanent_session_lifetime = timedelta(minutes=5)
  # Setting flask.session.modified resets the time after every request the user does so that
  # the session only expires after true inactivity.
  session.modified = True
