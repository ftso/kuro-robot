# Kuro Quadraped

Kuro is a quadraped robot with 8 motors (2 for each leg) and a camera. The end goal
is to be able to control the robot remotely to find my cat, Melody. Kuro is named after 
Kuromi in Santrio. In Santrio, Kuromi is Melody's rival.

For hardware and 3D printer STLs, see https://www.thingiverse.com/thing:3907678.
The code provided here uses Adafruit Servo Hat to control the servos on the motors, a picamera to
video stream, and a flask web UI to control the robot and see the videostream.

## Deployment
The server is run using gunicorn:

```shell
gunicorn -b localhost:8000 -w 4 wsgi
```

For monitoring and server startup, I use supervisor. There is a conf file located
in /etc/supervisor/conf.d. 

To start: 

```shell
sudo supervisorctl start kuro_robot
```

To update config:

```shell
sudo supervisorctl update
or
sudo supervisorctl reload
```

To stop: 

```shell
sudo supervisorctl stop kuro_robot
```

When code is updated:

  1. sudo supervisorctl stop kuro_robot
  1. flask db upgrade
  1. sudo supervisorctl start kuro_robot
