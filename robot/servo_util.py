from __future__ import division

PULSE_WIDTH_MIN = 0.5
PULSE_WIDTH_MAX = 2.5
FREQUENCY_HZ = 50
FREQUENCY_MS = (1/FREQUENCY_HZ)*1000 # hz to ms is 1/hz*1000
MAX_ANGLE = 270 # max angle the servo can move to
MIN_ANGLE = 0 # min angle the servo can move to

def dutyCycleMsToPercent(dutyCycleMs):
    return (dutyCycleMs/FREQUENCY_MS)*100
    
MAX_DUTY_CYCLE_PERCENT = dutyCycleMsToPercent(PULSE_WIDTH_MAX)
MIN_DUTY_CYCLE_PERCENT = dutyCycleMsToPercent(PULSE_WIDTH_MIN)

def convertAngleToDutyCyclePercent(angle):
    if (angle > MAX_ANGLE):
        angle = MAX_ANGLE
    elif (angle < MIN_ANGLE):
        angle = MIN_ANGLE
    dutyCyclePercentRange = MAX_DUTY_CYCLE_PERCENT - MIN_DUTY_CYCLE_PERCENT
    dutyCyclePercent = ((angle*dutyCyclePercentRange)/MAX_ANGLE) + MIN_DUTY_CYCLE_PERCENT
    print("angle: " + str(angle) + " dutyCyclePercent: " + str(dutyCyclePercent))
    return dutyCyclePercent
    
def translateRange(value, originalMin, originalMax, targetMin, targetMax):
    # Figure out how 'wide' each range is
    originalRange = originalMax - originalMin
    targetRange = targetMax - targetMin

    # Convert the left range into a 0-1 range (float)
    valueScaled = float(value - originalMin) / float(originalRange)

    # Convert the 0-1 range into a value in the right range.
    return targetMin + (valueScaled * targetRange)

