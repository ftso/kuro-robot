from __future__ import division
from adafruit_servokit import ServoKit
import time

class Robot: 
    servoKit = ServoKit(channels=16)

    SERVO_FRONT_RIGHT_TOP = servoKit.servo[0]
    SERVO_FRONT_RIGHT_BOT = servoKit.servo[1]
    SERVO_FRONT_LEFT_TOP = servoKit.servo[2]
    SERVO_FRONT_LEFT_BOT = servoKit.servo[3]
    SERVO_BACK_RIGHT_TOP = servoKit.servo[4]
    SERVO_BACK_RIGHT_BOT = servoKit.servo[5]
    SERVO_BACK_LEFT_TOP = servoKit.servo[6]
    SERVO_BACK_LEFT_BOT = servoKit.servo[7]

    ANGLE_REST_FRONT_RIGHT_TOP = 160
    ANGLE_REST_FRONT_LEFT_TOP = 90
    ANGLE_REST_BACK_LEFT_TOP = 90
    ANGLE_REST_BACK_RIGHT_TOP = 90
    ANGLE_REST_BOT = 45

    SWEEP_ANGLE_FRONT = 20 # size of step
    SWEEP_ANGLE_BACK = 35 # size of step

    ANGLE_RES = 10 # resolution

    def __init__(self):        
        # Rest position
        self.goToRestPos()
    
    # Move all legs to default rest position    
    def goToRestPos(self):
        self.moveServoToAngle(self.SERVO_FRONT_RIGHT_TOP, self.ANGLE_REST_FRONT_RIGHT_TOP)
        self.moveServoToAngle(self.SERVO_FRONT_RIGHT_BOT, self.ANGLE_REST_BOT)
        self.moveServoToAngle(self.SERVO_FRONT_LEFT_TOP, self.ANGLE_REST_FRONT_LEFT_TOP)
        self.moveServoToAngle(self.SERVO_FRONT_LEFT_BOT, self.ANGLE_REST_BOT)
        self.moveServoToAngle(self.SERVO_BACK_RIGHT_TOP, self.ANGLE_REST_BACK_RIGHT_TOP)
        self.moveServoToAngle(self.SERVO_BACK_RIGHT_BOT, self.ANGLE_REST_BOT)
        self.moveServoToAngle(self.SERVO_BACK_LEFT_TOP, self.ANGLE_REST_BACK_LEFT_TOP)
        self.moveServoToAngle(self.SERVO_BACK_LEFT_BOT, self.ANGLE_REST_BOT)
        time.sleep(0.5)
    
    def moveServoToAngle(self, servo, targetAngle):
        #print("Servo: " + str(servo) + " TargetAngle: " + str(targetAngle))
        servo.angle = targetAngle
        time.sleep(0.1)

    def liftLeg(self, servo):
        self.moveServoToAngle(servo, self.ANGLE_REST_BOT + 30)

    def restLeg(self, servo):
        self.moveServoToAngle(servo, self.ANGLE_REST_BOT)
         
    def walkOneStep(self):
        self.liftLeg(self.SERVO_BACK_RIGHT_BOT)
        self.moveServoToAngle(
          self.SERVO_BACK_RIGHT_TOP,
          self.ANGLE_REST_BACK_RIGHT_TOP - self.SWEEP_ANGLE_BACK)
        self.restLeg(self.SERVO_BACK_RIGHT_BOT)

        self.liftLeg(self.SERVO_BACK_LEFT_BOT)
        self.moveServoToAngle(
          self.SERVO_BACK_LEFT_TOP,
          self.ANGLE_REST_BACK_LEFT_TOP + self.SWEEP_ANGLE_BACK)
        self.restLeg(self.SERVO_BACK_LEFT_BOT)

        self.liftLeg(self.SERVO_FRONT_RIGHT_BOT)
        self.moveServoToAngle(
          self.SERVO_FRONT_RIGHT_TOP,
          self.ANGLE_REST_FRONT_RIGHT_TOP - self.SWEEP_ANGLE_FRONT)
        self.restLeg(self.SERVO_FRONT_RIGHT_BOT)

        self.liftLeg(self.SERVO_FRONT_LEFT_BOT)
        self.moveServoToAngle(
          self.SERVO_FRONT_LEFT_TOP,
          self.ANGLE_REST_FRONT_LEFT_TOP + self.SWEEP_ANGLE_FRONT)
        self.restLeg(self.SERVO_FRONT_LEFT_BOT)

        self.goToRestPos()

    def turnRightOneStep(self):
        self.liftLeg(self.SERVO_BACK_RIGHT_BOT)
        self.moveServoToAngle(
            self.SERVO_BACK_RIGHT_TOP,
            self.ANGLE_REST_BACK_RIGHT_TOP + self.SWEEP_ANGLE_BACK)
        self.restLeg(self.SERVO_BACK_RIGHT_BOT)

        self.liftLeg(self.SERVO_BACK_LEFT_BOT)
        self.moveServoToAngle(
            self.SERVO_BACK_LEFT_TOP,
            self.ANGLE_REST_BACK_LEFT_TOP + self.SWEEP_ANGLE_BACK)
        self.restLeg(self.SERVO_BACK_LEFT_BOT)

        self.liftLeg(self.SERVO_FRONT_RIGHT_BOT)
        self.moveServoToAngle(
            self.SERVO_FRONT_RIGHT_TOP,
            self.ANGLE_REST_FRONT_RIGHT_TOP + self.SWEEP_ANGLE_FRONT)
        self.restLeg(self.SERVO_FRONT_RIGHT_BOT)

        self.liftLeg(self.SERVO_FRONT_LEFT_BOT)
        self.moveServoToAngle(
          self.SERVO_FRONT_LEFT_TOP,
          self.ANGLE_REST_FRONT_LEFT_TOP + self.SWEEP_ANGLE_FRONT)
        self.restLeg(self.SERVO_FRONT_LEFT_BOT)

        self.goToRestPos()

    def turnLeftOneStep(self):
        self.liftLeg(self.SERVO_BACK_RIGHT_BOT)
        self.moveServoToAngle(
            self.SERVO_BACK_RIGHT_TOP,
            self.ANGLE_REST_BACK_RIGHT_TOP - self.SWEEP_ANGLE_BACK)
        self.restLeg(self.SERVO_BACK_RIGHT_BOT)

        self.liftLeg(self.SERVO_BACK_LEFT_BOT)
        self.moveServoToAngle(
            self.SERVO_BACK_LEFT_TOP,
            self.ANGLE_REST_BACK_LEFT_TOP - self.SWEEP_ANGLE_BACK)
        self.restLeg(self.SERVO_BACK_LEFT_BOT)

        self.liftLeg(self.SERVO_FRONT_RIGHT_BOT)
        self.moveServoToAngle(
            self.SERVO_FRONT_RIGHT_TOP,
            self.ANGLE_REST_FRONT_RIGHT_TOP - self.SWEEP_ANGLE_FRONT)
        self.restLeg(self.SERVO_FRONT_RIGHT_BOT)

        self.liftLeg(self.SERVO_FRONT_LEFT_BOT)
        self.moveServoToAngle(
          self.SERVO_FRONT_LEFT_TOP,
          self.ANGLE_REST_FRONT_LEFT_TOP - self.SWEEP_ANGLE_FRONT)
        self.restLeg(self.SERVO_FRONT_LEFT_BOT)

        self.goToRestPos()


