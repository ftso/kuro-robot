from kuro import db, login_manager
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timezone

class User(UserMixin, db.Model):
    __table_args__ = {'extend_existing': True}
    
    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index = True, unique = True)
    password_hash = db.Column(db.String(128))
    login_history = db.relationship('LoginHistory', lazy='dynamic')
    
    def __repr__(self):
        return '<User {0}, ID {1}>'.format(self.username, self.user_id)
        
    def get_id(self):
        return self.user_id
    
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)
        
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
    
class LoginHistory(db.Model):
    __table_args__ = {'extend_existing': True}
    
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), db.ForeignKey('user.username'))
    login_timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    
    def __repr__(self):
        return '<Login: {0}, Timestamp: {1}>'.format(self.username, 
          self.login_timestamp.replace(tzinfo=timezone.utc).astimezone(tz=None))
    

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))
