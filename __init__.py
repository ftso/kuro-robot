import sys, os
sys.path.append('/home/pi/Desktop')
sys.path.append('/home/pi/Desktop/kuro')

from flask import Flask, render_template, Response
from flask_sqlalchemy import SQLAlchemy
from flask_config import FlaskConfig
from flask_login import LoginManager

application = Flask(__name__)
SECRET_KEY = FlaskConfig.SECRET_KEY
application.config.from_object(FlaskConfig)
application.secret_key = SECRET_KEY
application.config['SECRET_KEY'] = SECRET_KEY

db = SQLAlchemy(application)

login_manager = LoginManager(application)
# Tell login manager which page handles login to automatically redirect
# to this page if the user is not authenticated.
login_manager.login_view = 'login'

from kuro import db_models,routes
