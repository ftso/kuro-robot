
from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField
from wtforms.validators import DataRequired

class RobotControllerForm(FlaskForm):
	numSteps = IntegerField('NumSteps', validators=[DataRequired()])
	forward = SubmitField('Forward')
	rotateRight = SubmitField('Rotate Right')
	rotateLeft = SubmitField('Rotate Left')
	
