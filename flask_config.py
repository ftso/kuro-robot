import os
basedir = os.path.abspath(os.path.dirname(__file__))

class FlaskConfig(object):
	SECRET_KEY = os.environ.get('FLASK_SECRET')
	# SQLite database path
	path = 'sqlite:///' + os.path.join(basedir, 'robot_controller.db')
	SQLALCHEMY_DATABASE_URI = path
	# Don't signal application everytime a change is about to be made in database
	SQLALCHEMY_TRACK_MODIFICATIONS = False
